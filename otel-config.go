package sbotel

import (
	"context"
	"log"
	"strings"
	"time"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	"go.opentelemetry.io/otel/exporters/stdout/stdouttrace"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc"
)

type OtelSbConfig struct {
	ServiceName string
	Grpc        OtelGrpcConfig
	Http        OtelHttpConfig
	Logging     OtelLogConfig
}

//Types

type OtelGrpcConfig struct {
	Enabled  bool
	Endpoint string
}

type OtelHttpConfig struct {
	Enabled  bool
	Endpoint string
}
type OtelLogConfig struct {
	Enabled bool
}

func InitConfiguration(conf OtelSbConfig) func() {
	ctx := context.Background()

	//Default settings
	otel.SetTracerProvider(trace.NewNoopTracerProvider())

	// Nom du provider
	serviceName := "changeTheServiceName_PLEASE"
	if len(conf.ServiceName) != 0 {
		serviceName = conf.ServiceName
	}

	res, err := resource.New(ctx, resource.WithAttributes(semconv.ServiceNameKey.String(serviceName)))
	handleErr(err, "failed to create resource")

	traceProviderOptions := []sdktrace.TracerProviderOption{
		sdktrace.WithSampler(sdktrace.AlwaysSample()),
		sdktrace.WithResource(res),
	}

	configuredExporter := 0
	if conf.Grpc.Enabled {
		configuredExporter++
		log.Println("Configuring Otel GRPC Exporter")
		var otlpOptions = []otlptracegrpc.Option{
			otlptracegrpc.WithInsecure(),
			otlptracegrpc.WithDialOption(grpc.WithBlock()),
			otlptracegrpc.WithDialOption(grpc.WithReturnConnectionError()),
		}

		if !isBlank(conf.Grpc.Endpoint) {
			otlpOptions = append(otlpOptions, otlptracegrpc.WithEndpoint(conf.Grpc.Endpoint))
		}

		// Set up a trace exporter
		grpcctx, _ := context.WithTimeout(ctx, 20*time.Second)
		traceExporter, err := otlptracegrpc.New(grpcctx, otlpOptions...)
		handleErr(err, "failed to create trace exporter")

		spanProcessor := sdktrace.WithSpanProcessor(sdktrace.NewBatchSpanProcessor(traceExporter))
		traceProviderOptions = append(traceProviderOptions, spanProcessor)
		log.Println("GRPC Exporter Configured")
	}

	if conf.Http.Enabled {
		configuredExporter++
		log.Println("Configuring Otel HTTP Exporter")
		var otlpOptions = []otlptracehttp.Option{
			otlptracehttp.WithInsecure(),
			otlptracehttp.WithEndpoint("localhost:55681"),
		}

		if isBlank(conf.Http.Endpoint) {
			otlpOptions = append(otlpOptions, otlptracehttp.WithEndpoint(conf.Http.Endpoint))
		}

		traceExporter, err := otlptracehttp.New(ctx, otlpOptions...)
		handleErr(err, "failed to create trace exporter")

		spanProcessor := sdktrace.WithSpanProcessor(sdktrace.NewBatchSpanProcessor(traceExporter))
		traceProviderOptions = append(traceProviderOptions, spanProcessor)
		log.Println("HTTP Exporter Configured")
	}

	if conf.Logging.Enabled {
		configuredExporter++
		log.Println("Configuring LOG Exporter")

		traceExporter, err := stdouttrace.New(stdouttrace.WithPrettyPrint())
		handleErr(err, "failed to create trace exporter")

		spanProcessor := sdktrace.WithSpanProcessor(sdktrace.NewBatchSpanProcessor(traceExporter))
		traceProviderOptions = append(traceProviderOptions, spanProcessor)
		log.Println("LOG Exporter Configured")
	}

	// set global propagator to tracecontext (the default is no-op).
	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}))

	// If at least 1 exporter is configured, then replace noop tracer provider with this one
	if configuredExporter > 0 {
		tracerProvider := sdktrace.NewTracerProvider(traceProviderOptions...)

		otel.SetTracerProvider(tracerProvider)

		return func() {
			// Shutdown will flush any remaining spans and shut down the exporter.
			handleErr(tracerProvider.Shutdown(ctx), "failed to shutdown TracerProvider")
		}
	}

	return func() {}
}

func handleErr(err error, message string) {
	if err != nil {
		log.Fatalf("%s: %v", message, err)
	}
}

func isBlank(s string) bool {
	return strings.TrimSpace(s) == ""
}
