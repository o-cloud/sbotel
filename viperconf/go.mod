module gitlab.com/o-cloud/sbotel/viperconf

go 1.16

require (
	github.com/spf13/viper v1.8.1
	gitlab.com/o-cloud/sbotel v0.0.0-20211021091451-dc3eaeda7dcd
)
