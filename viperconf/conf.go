package viperconf

import (
	"github.com/spf13/viper"
	"gitlab.com/o-cloud/sbotel"
)

type ViperOtelConfHelper struct {
	v *viper.Viper
}

func New() *ViperOtelConfHelper {
	return &ViperOtelConfHelper{v: viper.New()}
}

func (voch *ViperOtelConfHelper) WithEnvPrefix(prefix string) *ViperOtelConfHelper {
	voch.v.SetEnvPrefix(prefix)
	return voch
}

func (voch *ViperOtelConfHelper) WithEnv() *ViperOtelConfHelper {
	voch.v.BindEnv("servicename", "OTEL_SERVICE_NAME")
	voch.v.BindEnv("grpc.endpoint", "OTEL_GRPC_ENDPOINT")
	voch.v.BindEnv("grpc.enabled", "OTEL_GRPC")
	voch.v.BindEnv("http.endpoint", "OTEL_HTTP_ENDPOINT")
	voch.v.BindEnv("http", "OTEL_HTTP")
	voch.v.BindEnv("loggin", "OTEL_LOGGING")
	return voch
}

func (voch *ViperOtelConfHelper) WithFile(filePath string) *ViperOtelConfHelper {
	voch.v.SetConfigFile(filePath)
	return voch
}

func (voch *ViperOtelConfHelper) GetConfig() (sbotel.OtelSbConfig, error) {
	var config sbotel.OtelSbConfig
	if err := voch.v.ReadInConfig(); err != nil {
		return config, err
	}
	if err := voch.v.Unmarshal(&config); err != nil {
		return config, err
	}
	return config, nil
}
