package hammerpropagator

import (
	"context"

	"github.com/ShaileshSurya/hammer"
	"go.opentelemetry.io/otel"
)

func Propagate(ctx context.Context, req *hammer.Request) {
	otel.GetTextMapPropagator().Inject(ctx, HammerHeaderPropagator(*req))
}

type HammerHeaderPropagator hammer.Request

func (hhp HammerHeaderPropagator) Get(key string) string {
	return ""
}

func (hhp HammerHeaderPropagator) Set(key, value string) {
	req := hammer.Request(hhp)
	req.WithHeaders(key, value)
}

func (hhp HammerHeaderPropagator) Keys() []string {
	return []string{}
}
