module gitlab.com/o-cloud/sbotel/hammerpropagator

go 1.16

require (
	github.com/ShaileshSurya/hammer v1.0.2
	go.opentelemetry.io/otel v1.0.0-RC2
)
